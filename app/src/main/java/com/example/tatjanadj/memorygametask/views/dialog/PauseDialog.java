package com.example.tatjanadj.memorygametask.views.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

import com.example.tatjanadj.memorygametask.BaseActivity;
import com.example.tatjanadj.memorygametask.R;

public class PauseDialog extends BaseDialog {

    public PauseDialog(final BaseActivity activity, final PauseDialogListener listener) {
        super(activity, R.layout.dialog_pause_game);

        findViewById(R.id.btn_pause_dialog_resume_game).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.resumeGame();
                PauseDialog.this.cancel();
            }
        });

        findViewById(R.id.btn_specific_to_pause_dialog_choose_level).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.chooseLevel();
            }
        });

        findViewById(R.id.btn_specific_to_pause_dialog_restart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.restartGame();
            }
        });


}


public interface PauseDialogListener{
        void resumeGame();
        void chooseLevel();
        void restartGame();
    }

}