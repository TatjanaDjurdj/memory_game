package com.example.tatjanadj.memorygametask;
import com.example.tatjanadj.memorygametask.utils.ScreenUtils;
import com.example.tatjanadj.memorygametask.utils.SocialUtils;
import com.example.tatjanadj.memorygametask.views.MemoryCardView;
import com.example.tatjanadj.memorygametask.views.dialog.BaseDialog;
import com.example.tatjanadj.memorygametask.views.dialog.EndDialog;
import com.example.tatjanadj.memorygametask.views.dialog.PauseDialog;
import com.twitter.sdk.android.core.Twitter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

public class MemoryGameActivity extends BaseActivity implements View.OnClickListener {

    public static final String LEVEL_KEY = "beginner_key";
    private int yyy;
    private int numOfOpenedCards;
    private int numOfClicks;
    private int row;
    private MemoryCardView memoryCardFirstViewHolder;
    private MemoryCardView memoryCardSecondViewHolder;
    private MemoryCardView memoryCardViewHolder;
    private Handler mHandler;
    public TextView numOfClicksView, timerView;
    public Button pause;
    int seconds = 0;
    int minutes = 0;
    boolean isPaused=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memory_game);
        MainActivity.Level level = (MainActivity.Level) getIntent().getSerializableExtra(LEVEL_KEY);

        row = level.rows;

        numOfClicks = 0;
        numOfOpenedCards = 0;

        Twitter.initialize(this);
        GridLayout gridLayout = findViewById(R.id.gridview);
        gridLayout.setColumnCount(row);
        gridLayout.setRowCount(row);

// set textview for displaying number of clicks
        numOfClicksView = findViewById(R.id.numofclicksview);
        numOfClicksView.setText("Clicks : " + numOfClicks);
        numOfClicksView.setTextColor(Color.WHITE);
        numOfClicksView.setTextSize(20);
        numOfClicksView.setGravity(Gravity.TOP);
        numOfClicksView.setGravity(Gravity.START);

        // set button for pause or resume
        pause = findViewById(R.id.pausebutton);
        pause.setWidth(ScreenUtils.dpToPx(120));
        pause.setHeight(ScreenUtils.dpToPx(40));
        pause.setText("PAUSE");
        pause.setBackgroundColor(Color.GRAY);
        pause.setTextColor(Color.WHITE);
        pause.setOnClickListener(new View.OnClickListener() {
                                     @Override
                                     public void onClick(View v) {
                                         isPaused = true;
                                         new PauseDialog(MemoryGameActivity.this, new PauseDialog.PauseDialogListener() {
                                             @Override
                                             public void resumeGame() {
                                                 isPaused = false;


                                             }

                                             @Override
                                             public void chooseLevel() {
                                                 Intent intentChooseLevel = new Intent(MemoryGameActivity.this, MainActivity.class);
                                                 startActivity(intentChooseLevel);

                                             }

                                             @Override
                                             public void restartGame() {
                                                 recreate();

                                             }
                                         });
                                     }
                                 });








        timerView = findViewById(R.id.timerview);
        timerView.setTextSize(20);
        timerView.setTextColor(Color.WHITE);

        Timer t = new Timer();
        //Set the schedule function and rate
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (!isPaused){
                        timerView.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));
                        seconds += 1;

                        if (seconds == 60) {
                            timerView.setText(String.valueOf(minutes) + ":" + String.valueOf(seconds));

                            seconds = 0;
                            minutes = minutes + 1;}


                        }
                    }

                });
            }

        }, 0, 1000);


        ArrayList<String> cardElements = new ArrayList<>();

        for (int y = 1; y <= (row * row / 2); y++) {
            cardElements.add(Integer.toString(y));
        }

        cardElements.addAll(cardElements);
        Collections.shuffle(cardElements);

        for (int i = 0; i < row * row; i++) {
            MemoryCardView memoryCard = new MemoryCardView(this);
            memoryCard.setFrontSide(cardElements.get(i));
            memoryCard.close();
            memoryCard.setOnClickListener(this);
            gridLayout.addView(memoryCard);
        }

    }

    @Override
    public void onClick(View view) {
        memoryCardViewHolder = (MemoryCardView) view;

        // Check if card is already opened
        if (memoryCardViewHolder.isOpen()) {
            Toast.makeText(MemoryGameActivity.this, "Card is already open,please choose another", Toast.LENGTH_SHORT).show();
        } else {

            // Close...
            if (isFirstCardActive() && isSecondCardActive()) {
                memoryCardFirstViewHolder.close();
                memoryCardSecondViewHolder.close();
                mHandler.removeCallbacksAndMessages(null);
            }

            memoryCardViewHolder.open();
            yyy++;
            numOfClicks++;
            numOfClicksView.setText("Clicks : " + numOfClicks);


            if (yyy == 1) {
                memoryCardFirstViewHolder = memoryCardViewHolder;
            } else if (yyy == 2) {

                memoryCardSecondViewHolder = memoryCardViewHolder;

                if (memoryCardFirstViewHolder.getFrontSide().equals(memoryCardSecondViewHolder.getFrontSide())) {
                    Toast.makeText(MemoryGameActivity.this, "GOOD JOB ", Toast.LENGTH_SHORT).show();
                    memoryCardFirstViewHolder.setEnabled(false);
                    memoryCardSecondViewHolder.setEnabled(false);

                    numOfOpenedCards++;
                    yyy = 0;
                    checkIfEnded();
                } else {

                    mHandler = new Handler();
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            memoryCardFirstViewHolder.close();
                            memoryCardSecondViewHolder.close();
                        }
                    }, 1000);
                    yyy = 0;
                }
            }
        }
    }


    private boolean isFirstCardActive() {
        return memoryCardFirstViewHolder != null && memoryCardFirstViewHolder.isOpen() && memoryCardFirstViewHolder.isEnabled();
    }


    private boolean isSecondCardActive() {
        return memoryCardSecondViewHolder != null && memoryCardSecondViewHolder.isOpen() && memoryCardSecondViewHolder.isEnabled();
    }

    public void checkIfEnded() {
        if (numOfOpenedCards == row * row / 2) {
            isPaused=true;
            final String score="My score is "+numOfClicks+" in "+minutes+":"+seconds+" Try to beat me if you can.Click to downlad the game.";
            final String site="https://bitbucket.org/TatjanaDjurdj/";

                            new EndDialog(MemoryGameActivity.this, new EndDialog.EndDialogListener() {
                    @Override
                    public void shareToFbook() {
                        SocialUtils.shareToFb(site,score,MemoryGameActivity.this);

                    }

                    @Override
                    public void shareToTwitter() {
                        SocialUtils.shareToTw(score,MemoryGameActivity.this);

                    }
                });


//

        }



    }
}







