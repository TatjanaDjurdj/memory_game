package com.example.tatjanadj.memorygametask.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridLayout;

public class MemoryCardGridView extends GridLayout {

    public MemoryCardGridView(Context context) {
        super(context);
    }

    public MemoryCardGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MemoryCardGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
