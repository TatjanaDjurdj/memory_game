package com.example.tatjanadj.memorygametask.utils;

import android.app.Activity;
import android.net.Uri;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

public class SocialUtils {

    public static void shareToFb(String score, String site, Activity activity) {
        ShareDialog shareDialog = new ShareDialog(activity);

        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(site))
                .setQuote(score).build();
        shareDialog.show(linkContent);

    }

    public static void shareToTw(String score, Activity activity) {
        TweetComposer.Builder builder = new TweetComposer.Builder(activity)
                .text(score);
        builder.show();
    }

}
