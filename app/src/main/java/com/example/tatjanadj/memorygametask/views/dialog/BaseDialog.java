package com.example.tatjanadj.memorygametask.views.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.View;
import android.widget.FrameLayout;

import com.example.tatjanadj.memorygametask.R;

public class BaseDialog extends AlertDialog {

    private final View viewCancel;
    private FrameLayout mContentLayout;

    public BaseDialog(final Activity context, final int resourceLayoutId){
        super(context);
        show();
        setContentView(R.layout.base_dialog);

        mContentLayout = findViewById(R.id.content_layout);
       View view =  getLayoutInflater().inflate(resourceLayoutId, null);

       mContentLayout.addView(view);

        getWindow().setLayout(600, 400);

        viewCancel =findViewById(R.id.buttonCancel);
        viewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseDialog.this.cancel();
            }
        });
    }

}
