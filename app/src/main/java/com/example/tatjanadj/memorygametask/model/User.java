package com.example.tatjanadj.memorygametask.model;

public class User {

    private int id;
    private String name;
    private int clickCount;
    private int time;

    public User(int id, String name, int clickCount, int time) {
        this.id = id;
        this.name = name;
        this.clickCount = clickCount;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getClickCount() {
        return clickCount;
    }

    public void setClickCount(int clickCount) {
        this.clickCount = clickCount;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
