package com.example.tatjanadj.memorygametask.views.dialog;

import android.view.View;

import com.example.tatjanadj.memorygametask.BaseActivity;
import com.example.tatjanadj.memorygametask.R;

public class EndDialog extends BaseDialog {

    public EndDialog(final BaseActivity activity, final EndDialogListener listener) {
        super(activity, R.layout.dialog__end_game);

        findViewById(R.id.btn_specific_to_end_dialog_share_to_fb).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.shareToFbook();


            }
        });
        findViewById(R.id.btn_specific_to_end_dialog_share_to_twitter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.shareToTwitter();
            }
        });

    }


    public interface EndDialogListener {

        void shareToFbook();

        void shareToTwitter();

    }

}
