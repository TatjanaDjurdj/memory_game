package com.example.tatjanadj.memorygametask.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.example.tatjanadj.memorygametask.utils.ScreenUtils;

public class MemoryCardView extends android.support.v7.widget.AppCompatTextView {

    private String mFrontSide;
    private String mBackSide;
    private boolean mIsOpened;

    public MemoryCardView(Context context) {
        super(context);
        initView();
    }

    public MemoryCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();

    }

    public MemoryCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void setFrontSide(final String frontSide) {
        mFrontSide = frontSide;
    }

    public String getFrontSide() {
        return mFrontSide;
    }


    public boolean isOpen() {
        return mIsOpened;
    }

    public void open() {
        mIsOpened = true;
        setTextColor(Color.GREEN);
        setText(mFrontSide);

    }

    public void close() {
        mIsOpened = false;
        setTextColor(Color.BLACK);
        setText(mBackSide);
    }

    public void initView() {
        mBackSide = "?";
        setText(mBackSide);

        setWidth(ScreenUtils.dpToPx(40));
        setHeight(ScreenUtils.dpToPx(40));
        setTextSize(15);
        setGravity(Gravity.CENTER);
        setTextColor(Color.BLACK);
        setBackgroundColor(Color.GRAY);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout
                .LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        layoutParams.topMargin = 5;
        layoutParams.bottomMargin = 5;

        setLayoutParams(layoutParams);


    }
}
