package com.example.tatjanadj.memorygametask;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends BaseActivity {
    Button fourButton, sixButton, eightButton  ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fourButton =findViewById(R.id.button4x4);
        sixButton=findViewById(R.id.button6x6);
        eightButton=findViewById(R.id.button8x8);


        fourButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentBeginner=new Intent(MainActivity.this, MemoryGameActivity.class);
                intentBeginner.putExtra(MemoryGameActivity.LEVEL_KEY, Level.BEGINNER);
                startActivity(intentBeginner);
            }
        });

        sixButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentMedium=new Intent(MainActivity.this, MemoryGameActivity.class);
                intentMedium.putExtra(MemoryGameActivity.LEVEL_KEY, Level.MEDIUM);
                startActivity(intentMedium);
            }
        });
        eightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentHard=new Intent(MainActivity.this, MemoryGameActivity.class);
                intentHard.putExtra(MemoryGameActivity.LEVEL_KEY, Level.HARD);
                startActivity(intentHard);
            }
        });

    }


    public enum Level{
        BEGINNER(4),
        MEDIUM(6),
        HARD(8);

        public int rows;

        Level(final int rows){
            this.rows = rows;
        }
    }
}
